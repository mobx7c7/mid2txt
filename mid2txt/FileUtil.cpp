#include "FileUtil.h"

#include <fstream>

int ReadFile(std::string filePath, std::vector<char> &fileData)
{
	std::ifstream ifs(filePath);

	if(!ifs)
	{
		return 0;
	}

	ifs.seekg(0, ifs.end);
	size_t fileSize = ifs.tellg();
	ifs.seekg(0, ifs.beg);

	if(fileSize == 0)
	{
		return 0;
	}

	fileData.resize(fileSize);

	ifs.read(&fileData[0], fileSize);

	return 1;
}