#include "MidiFileParser.h"

#include <iostream>
#include <sstream>
#include <bitset>

#define MSB_TO_LSB_INT(x) ((x >> 24 & 0xFF) | ((x >> 16 & 0xff) << 8) | ((x >> 8 & 0xff) << 16) | ((x & 0xFF) << 24))
#define MSB_TO_LSB_SHORT(x) ((x >> 8 & 0xFF) | ((x & 0xff) << 8))

MidiFileParser::MidiFileParser(void)
{
}

MidiFileParser::~MidiFileParser(void)
{
}

int ReadVLV(std::vector<char> &data, size_t &off)
{
	// Valor maximo = 0xFFFFFFF
	// Limite definido para ser manipulado em valores de 32-bits = 4 bytes.
	// 
	// The largest value allowed within a MIDI file is 0FFFFFFF. 
	// This limit is set to allow variable-length quantities 
	// to be manipulated as 32-bit integers.

	/*
	
	int test = 0x6887; // 10000111 01101000
	char* cc = (char*)&test; //&data[off+vlq];

	int value = 0;
	char c;

	if ((value = *cc++) & 0x80)
	{
		value &= 0x7f;
		do
		{
			value = (value << 7) + (c = *cc++ & 0x7f);
		}
		while (c & 0x80);
	}
	// TESTE OK! resulta em 1000
	return (value);
	*/

	int value = 0;
	char c;

	if ((value = data[off++]) & 0x80)
	{
		value &= 0x7f;
		do
		{
			value = (value << 7) + ((c = data[off++]) & 0x7f);
		}
		while (c & 0x80);
	}
	return value;
}

std::string ParseChunkSignature(std::vector<char> &data, size_t &off)
{
	std::string magic = std::string(&data[off], 4);
	off+=4;
	return magic;
}

std::string ParseTrackMidiEvent(std::vector<char> &data, size_t &off)
{
	std::stringstream ss;
	std::cout << "Midi event" << std::endl;

	unsigned status = data[off-1] & 0xFF; // Gambi mode on!
	std::cout << "Status=0x" << std::hex << status << std::endl;

	unsigned data1 = data[off++] & 0xFF;
	unsigned data2 = data[off++] & 0xFF;
	std::cout << "data1=" << std::hex << data1 << std::endl;
	std::cout << "data2=" << std::hex << data2 << std::endl;

	return ss.str();
}

std::string ParseTrackSysexEvent(std::vector<char> &data, size_t &off)
{
	std::stringstream ss;
	std::cout << "Sysex event" << std::endl;
	return ss.str();
}

std::string ParseTrackMetaEvent(std::vector<char> &data, size_t &off)
{
	std::stringstream ss;

	ss << "Meta event" << std::endl;

	unsigned type = data[off++];
	unsigned length = ReadVLV(data, off);

	//std::cout << "\t\t0x" << std::hex << type << ", 0x" << length << std::endl;

	// TODO: Implementa��o de cada evento;
	switch(type)
	{
	case 0x0:
		ss << "Sequence Number";
		break;



	case 0x1:
		ss << "Text Event";
		//off+=length;
		break;
	case 0x2:
		ss << "Copyright Notice";
		//off+=length;
		break;
	case 0x3:
		ss << "Sequence/Track Name";
		//std::cout << std::string(&data[off], length) << std::endl;
		//off+=length;
		break;
	case 0x4:
		ss << "Instrument Name";
		//off+=length;
		break;
	case 0x5:
		ss << "Lyric";
		//off+=length;
		break;
	case 0x6:
		ss << "Marker";
		//off+=length;
		break;
	case 0x7:
		ss << "Cue Point";
		//off+=length;
		break;



	case 0x20:
		ss << "MIDI Channeel Prefix";
		break;
	case 0x2f:
		ss << "End of Track";
		break;
	case 0x51:
		ss << "Set Tempo";
		//off+=length; // temp
		break;
	case 0x54:
		ss << "SMPTE Offset";
		break;
	case 0x58:
		ss << "Time Signature";
		//off+=length; // temp
		break;
	case 0x59:
		ss << "Signature";
		break;
	case 0x7f:
		ss << "Sequencer Specific Meta-Event";
		break;
	default:
		ss << "?";
	}

	ss << std::endl;

	off += length;

	return ss.str();
}

std::string ParseTrackEvent(std::vector<char> &data, size_t &off)
{
	std::stringstream ss;

	bool lastEvent = false;

	while(!lastEvent)
	{
		unsigned deltaTime	= ReadVLV(data, off);
		unsigned trackEvent = data[off++] & 0xff;

		std::cout << "DeltaTime=" << std::dec << deltaTime << std::endl;

		switch(trackEvent)
		{
		case 0xFF: // meta
			{
				lastEvent = (data[off] & 0xFF) == 0x2f; // End of track
				std::cout << ParseTrackMetaEvent(data, off);
				break;
			}
		case 0xF0: // sysex
		case 0xF7: // sysex
			{
				std::cout << ParseTrackSysexEvent(data, off);
				break;
			}
		default: // midi
			{
				std::cout << ParseTrackMidiEvent(data, off);
				std::cin.get();
			}
		};
		std::cout << std::endl;
	}

	// Atual: NULL (End of track)
	// Prox: Inicio de um outro poss�vel bloco.
	//off++; 
	
	return ss.str();
}

std::string ParseTrackData(std::vector<char> &data, size_t &off)
{
	std::stringstream ss;

	//while(off < data.size())
	{
		std::string chunkSignature = ParseChunkSignature(data, off);

		std::cout << "------------Chunk=" << chunkSignature << "\n";

		if(chunkSignature == "MTrk")
		{
			size_t chunkSize = MSB_TO_LSB_INT(*(int*)&data[off]);
			off+=4;

			std::cout << "------------Size=" << chunkSize << "\n";

			std::cout << "Chunk=" << chunkSignature << std::endl;
			std::cout << "Length=" << chunkSize << std::endl;
			std::cout << std::endl;

			//if(chunkSize > 0) // opcional
			{
				std::cout << ParseTrackEvent(data, off);
			}

			std::cin.get();
		}
	}
	
	return ss.str();
}

std::string ParseHeadData(std::vector<char> &data, size_t &off)
{
	std::stringstream ss;

	std::string chunkSignature = ParseChunkSignature(data, off);

	if(chunkSignature == "MThd")
	{
		size_t chunkLength = MSB_TO_LSB_INT(*(int*)&data[off]);
		off+=4;

		std::cout << "Chunk=" << chunkSignature << "\n";
		std::cout << "Length=" << chunkLength << "\n";
		std::cout << std::endl;

		if(chunkLength == 6) // deve ser exatos 6!
		{
			std::cout << "Format=" << MSB_TO_LSB_SHORT(*(short*)&data[off]) << "\n";
			off+=2;
			std::cout << "Tracks=" << MSB_TO_LSB_SHORT(*(short*)&data[off]) << "\n";
			off+=2;

			short division = MSB_TO_LSB_SHORT(*(short*)&data[off]);
			short format = division & 1;
			off+=2;

			if(!format)
			{
				std::cout << "Ticks=" << division << "\n";
			}
			else
			{
				//ss << "Frames per second:\n";
				//ss << "Ticks per frame:\n";
			}
		}
		std::cout << std::endl;
	}

	return ss.str();
}

int MidiFileParser::Decode(std::vector<char> &src, std::string &dst)
{
	// cada chunk possui:
	// - 4 bytes	type (ex:MThd)
	// - 4 bytes	length
	// - n bytes	data
	
	// Rotina:
	// - checa chunk MThd
	// - checa chunk MTrk

	std::stringstream ss;
	std::string chunkSignature;
	size_t off = 0;

	while(off < src.size())
	{
		// FIXME: retirar daqui
		chunkSignature = std::string(&src[off], 4);

		// FIXME: retorno dever� ser flag para "parsing ok"
		if(chunkSignature == "MThd")
		{
			ss << ParseHeadData(src, off);
		}
		else if(chunkSignature == "MTrk")
		{
			ss << ParseTrackData(src, off);
		}
		else
		{
			off++;
		}
		
	}

	dst = ss.str();

	return 0;
}