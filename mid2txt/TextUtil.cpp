#include "TextUtil.h"

std::string GetFileExtension(std::string line)
{
	size_t off = line.find_last_of(".");
	if(off != -1)
		return line.substr(off+1, line.size()-off);
	else
		return std::string();
}

std::string GetFileName(std::string line)
{
	size_t off = line.find_last_of("\\");
	if(off != -1)
		return line.substr(off+1, line.size()-off);
	else
		return std::string();
}

std::string GetFilePath(std::string line)
{
	size_t off = line.find_last_of("\\");
	if(off != -1)
		return line.substr(0, off+1);
	else
		return std::string();
}
