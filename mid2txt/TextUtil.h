#pragma once

#include <string>

std::string GetFileExtension(std::string line);

std::string GetFileName(std::string line);

std::string GetFilePath(std::string line);
