#pragma once

#include <string>
#include <vector>
#include "MidiFileParser.h"

using namespace std;

class App
{
private:
	std::vector<std::string> m_cmdArgs;
public:
	App(int argc, char** argv);
	~App(void);
	int Execute();
};

