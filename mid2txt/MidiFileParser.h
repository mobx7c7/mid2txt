#pragma once

#include <string>
#include <vector>

class MidiFileParser
{
public:
	MidiFileParser(void);
	~MidiFileParser(void);
	int Decode(std::vector<char> &src, std::string &dst);
};

	/*
	Meta-events initially defined include:
	FF 00 02 					Sequence Number
	FF 01 len text 				Text Event
	FF 02 len text 				Copyright Notice
	FF 03 len text 				Sequence/Track Name
	FF 04 len text 				Instrument Name
	FF 05 len text 				Lyric
	FF 06 len text 				Marker
	FF 07 len text 				Cue Point
	FF 20 01 cc 				MIDI Channeel Prefix
	FF 2F 00 					End of Track
	FF 51 03 tt tt tt 			Set Tempo(in microseconds per MIDI quarter-note)
	FF 54 05 hr mn se fr ff 	SMPTE Offset
	FF 58 04 nn dd cc bb 		Time Signature
	FF 59 02 sf mi Key 			Signature
	FF 7F len data 				Sequencer Specific Meta-Event
	*/