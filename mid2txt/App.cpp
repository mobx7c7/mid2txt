#include "App.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include "TextUtil.h"
#include "FileUtil.h"

App::App(int argc, char** argv)
{
	m_cmdArgs.resize(argc);
	for(int i=0; i<argc; i++)
		m_cmdArgs[i] = *argv++;
}

App::~App(void)
{
}

int App::Execute()
{
	cout << "MidiToText" << endl;

	if(m_cmdArgs.size() < 2)
	{
		cout << "Sem argumentos" << endl;
		//return -1;
	}
	else
	{
		cout << "Parametros disponiveis:" << endl;
		for(int i=1; i<m_cmdArgs.size(); i++)
			cout << m_cmdArgs[i] << endl;
	}

	cout << endl;

	/////////////////////////////////////////////

	string filePath = "..\\batera.mid";
	vector<char> fileData;
	ReadFile(filePath, fileData);

	if(fileData.empty())
	{
		return -1;
	}

	cout << "Arquivo:" << filePath << endl;
	cout << "Tamanho:" << fileData.size() << endl;

	//for(int i=0; i<fileData.size(); i++)
	{
		//char c = fileData[i];
		//cout << (c > 32 ? c : '*');
	}

	/////////////////////////////////////////////

	MidiFileParser parser;
	string midiText;
	parser.Decode(fileData, midiText);

	if(midiText.empty())
	{
		//return -1;
	}

	cout << midiText << endl;

	/////////////////////////////////////////////

	cin.get();

	return 0;
}

int main(int argc, char** argv)
{
	App app(argc, argv);
	return app.Execute();
}